import org.Data.Currencies;
import org.Data.UserInputModel;
import org.Services.CurrencyValidation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.example.CurrencyConversion;

import java.io.InvalidClassException;

public class ConversionTest {
    private static CurrencyConversion convertCurrency;

    @BeforeAll
    public  static void initalizeData (){
      convertCurrency = new CurrencyConversion(new CurrencyValidation(Currencies.getInstance().getData()));
    }

    // whether Input is valid
    @Test
    public void givenInvalidInput_whenConvert_throwNullException() {

        UserInputModel model = new UserInputModel(35, null , "");

        Assertions.assertThrows(NullPointerException.class, () -> convertCurrency.convertCurrency(model),"Empty Input");

        model.setFromCurrency("dddd");
        model.setToCurrency("dddd");

        Assertions.assertThrows(InvalidClassException.class, () -> convertCurrency.convertCurrency(model),
                "Longer than 3 chars");
    }

    // Testin the Result
    @Test
    public void givenUnexpectedResult_whenConvert_throwInvalidTypeException() {
      double expectedResult = 78.19999999999999;
      UserInputModel model = new UserInputModel(34, "JOD", "EUR");
        try {
            convertCurrency.convertCurrency(model);
        } catch (InvalidClassException e) {
            throw new RuntimeException(e);
        }
        Assertions.assertEquals(expectedResult, model.getAmount());
    }
}

