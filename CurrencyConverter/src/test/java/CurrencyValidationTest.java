import org.Data.Currencies;
import org.Data.UserInputModel;
import org.Services.CurrencyValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.InvalidClassException;

public class CurrencyValidationTest {
    // whether Currency exists
    private static CurrencyValidation validation;

    @BeforeAll
    public  static void initalizeData (){
        validation = new CurrencyValidation(Currencies.getInstance().getData());
    }

    @Test
    public void givenInvalidCurrency_whenConvert_throwInvalidException() {

        Assertions.assertThrows(InvalidClassException.class, () -> validation.checkValidCurrencies(new UserInputModel(34,
                        "ads", "JOD")),
                                  "Currency doesn't exist");
    }
}
