package org.Services;
import org.Abstractions.ICurrencyValidation;
import org.Data.Currency;
import org.Data.UserInputModel;

import java.io.InvalidClassException;
import java.util.List;
import java.util.Objects;

public class CurrencyValidation implements  ICurrencyValidation {

    private Currency Original;
    private Currency Converted;
    private List<Currency> Data;

    // DI
    public CurrencyValidation(List<Currency> data){
        Data = data;
    }
    // TODO test this to throw Currency doesn't exist exception.
    public boolean checkValidCurrencies(UserInputModel Model) throws InvalidClassException {
        for (Currency datum : Data) {
            if (Objects.equals(Model.getFromCurrency(), datum.getClass().getSimpleName())) {
                Original = datum;
            }
            if (Objects.equals(Model.getToCurrency(), datum.getClass().getSimpleName())) {
                Converted = datum;
            }
        }
        if (Original == null || Converted == null )  throw new InvalidClassException("Currency doesn't exist");
        return Original != null && Converted != null;
    }
    public  Currency getConverted(){
        return Converted;
    }
}
