package org.Data;

public abstract class Currency {
    protected   double ExchangeRate;
    protected String CurrencyType;

    public abstract double getExchangeRate();

    public abstract void setExchangeRate(int exchangeRate);
    public abstract String getCurrencyType();

    public abstract void setCurrencyType(String currencyType);
}
