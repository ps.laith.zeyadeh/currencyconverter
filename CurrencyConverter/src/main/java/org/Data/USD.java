package org.Data;

public class USD extends Currency{
    public USD (){
        ExchangeRate = 2323;
        CurrencyType = "USD";
    }
    @Override
    public double getExchangeRate() {
        return ExchangeRate;
    }
    @Override
    public String getCurrencyType() {
        return CurrencyType;
    }
    @Override
    public  void setCurrencyType(String currencyType){
        CurrencyType = currencyType;
    }
    @Override
    public  void setExchangeRate(int exchangeRate){
      ExchangeRate = exchangeRate;
    }
}
