package org.Data;

import java.util.ArrayList;
import java.util.List;

public  class Currencies {
    private static Currencies single_instance = null;

    private Currencies()
    {
    }
    public static Currencies getInstance()
    {
        if (single_instance == null) single_instance = new Currencies();
        return single_instance;
    }
    public List<Currency> getData (){

        List<Currency> currencies = new ArrayList<Currency>();
        currencies.add(new BHD());
        currencies.add(new EUR());
        currencies.add(new JOD());
        currencies.add(new USD());

        return currencies;
    }
}