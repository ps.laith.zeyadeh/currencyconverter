package org.Data;

public class BHD extends Currency{
    public BHD (){
      ExchangeRate = 2.6;
      CurrencyType = "BHD";
    }
    @Override
    public double getExchangeRate() {
        return ExchangeRate;
    }
    @Override
    public String getCurrencyType() {
        return CurrencyType;
    }
    @Override
    public  void setCurrencyType(String currencyType){
        CurrencyType = currencyType;
    }
    @Override
    public  void setExchangeRate(int exchangeRate){
        ExchangeRate = exchangeRate;
    }
}
