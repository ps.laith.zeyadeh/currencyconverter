package org.Data;

public class EUR extends Currency{
    public EUR (){
        ExchangeRate = 2.3;
        CurrencyType = "EUR";
    }
    @Override
    public double getExchangeRate() {
        return ExchangeRate;
    }
    @Override
    public String getCurrencyType() {
        return CurrencyType;
    }
    @Override
    public  void setCurrencyType(String currencyType){
       CurrencyType = currencyType;
    }
    @Override
    public  void setExchangeRate(int exchangeRate){
         ExchangeRate = exchangeRate;
    }
}
