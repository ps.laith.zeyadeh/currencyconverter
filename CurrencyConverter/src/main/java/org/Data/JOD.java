package org.Data;

public class JOD extends Currency {
    public JOD (){
        ExchangeRate = 33;
        CurrencyType = "JOD";
    }
    @Override
    public double getExchangeRate() {
        return ExchangeRate;
    }
    @Override
    public String getCurrencyType() {
        return CurrencyType;
    }
    @Override
    public  void setCurrencyType(String currencyType){
        CurrencyType = currencyType;
    }
    @Override
    public  void setExchangeRate(int exchangeRate){
         ExchangeRate = exchangeRate;
    }
}
