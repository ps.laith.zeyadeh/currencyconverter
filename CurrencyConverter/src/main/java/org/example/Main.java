package org.example;

import org.Data.Currencies;
import org.Data.UserInputModel;
import org.Services.CurrencyValidation;

import java.io.InvalidClassException;

public class Main {
    public static void main(String[] args) {
        try {
            new CurrencyConversion(new CurrencyValidation(Currencies.getInstance().getData())).
                    convertCurrency(new UserInputModel(34, "JOD", "EUR"));
        } catch (InvalidClassException e) {
            throw new RuntimeException(e);
        }
    }
}