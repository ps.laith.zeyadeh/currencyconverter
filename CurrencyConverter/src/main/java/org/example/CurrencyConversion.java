package org.example;

import org.Data.*;
import java.io.InvalidClassException;
import org.Services.CurrencyValidation;

// Big decimal

public class CurrencyConversion {

    private CurrencyValidation validator;

    public CurrencyConversion(CurrencyValidation Validator) {
      validator = Validator;
    }
    public void convertCurrency(UserInputModel Model) throws InvalidClassException {

        if (Model.getFromCurrency().equals("") || Model.getFromCurrency().equals(null) ||
                      Model.getToCurrency().equals("") || Model.getToCurrency().equals(null))
            throw new NullPointerException("Empty Input");

        if (Model.getFromCurrency().length() > 3 || Model.getToCurrency().length() > 3) {
            throw new InvalidClassException("Longer than 3 characters");
        }
        if (validator.checkValidCurrencies(Model)) { // should've given warning for an exception to be thrown
            Model.setAmount(Model.getAmount() * validator.getConverted().getExchangeRate());
            System.out.println(Model.getAmount());
        }
    }
}
