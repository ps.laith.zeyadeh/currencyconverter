package org.Abstractions;
import org.Data.UserInputModel;

import java.io.InvalidClassException;

public interface ICurrencyValidation {
   public boolean checkValidCurrencies(UserInputModel Model) throws InvalidClassException;
}
